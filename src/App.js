import React, { Component } from "react";
import "./App.css";
import points from "./points.json";
import Banner from "./banner";

class App extends Component {
  render() {
    const arr = [1,2,3,undefined,5];

      const newArr = arr.filter((x) => x)
      console.log('newArr',newArr);


    if (!points) return;
    let result = [];
    points.filter((x) => x).map(point => {
      result.push(point.itemSellType && `this item is ${point.itemSellType} purchase type `)
      result.push(point.diamond && `diamond points up ${point.diamond} `)
      result.push(point.platinum && `platinum points up ${point.platinum} `)
      result.push(point.gold && `gold points up ${point.gold} `)
      result.push(point.silver && `silver points up ${point.silver} `)
      result.push(point.all && `all levels points up ${point.all} `)
      return result.filter((x) => x).join(' ');
    });
    console.log('result',result);

    return (
      <Banner banner={result} />
    );
  }
}

export default App;
