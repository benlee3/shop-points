import React from "react";

function banner (props) {
  return (
    <div>
      <h1>{props.banner}</h1>
    </div>
  )
}

export default banner;
